<?php

/*
	efc-debug-include.php
	
	If you want to be able to create messages for the debug menu
	from a part of WordPress that is loaded before the plugins are
	loaded, then include this file early on...
	
	include_once(dirname(__FILE__).'/../../plugins/efc-debug/efc-debug-include.php');
		
	...assuming standard WordPress directory structure.
	
	Then you will be able to add messages to the Debug Report with...
	
	if (function_exists('dbug_report')) dbug_report('message: ',$variable);
	
	However, unless the plugin is activated, you won't ever see the results of 
	these reports.
*/

require_once('kint/Kint.class.php');

/*
	When updating Kint.class.php remember to always
	-	add name of dbug_report to test in _getPassedNames with
				strtolower( $callee['function'] ) === 'dbug_report' ||
	-	preserve the config.php for Kint
	-	preserve the efc-debug.css for Kint
	-	remove the Kint .gitignore (to pass config.php)
*/

if ( ! function_exists('dbug_report') ) {

	// silence the usual warning about depricated constructors,
	// but replace it with an even more meaningful backtrace from our debugger
	if ( WP_DEBUG ) add_action('deprecated_constructor_run', 'dbug_deprecated_constructor_run', 1, 2);
	function dbug_deprecated_constructor_run( $class, $version ) {
		add_filter('deprecated_constructor_trigger_error', '\\__return_false');
		dbug_report('depricated constructor',$class,$version);
		dbug_backtrace();
	}

	function dbug_report() {
		
		ob_start();
		if ( Kint::enabled() ) {
			call_user_func_array( array( 'Kint', 'dump' ), func_get_args() );
			dbug_add_note( ob_get_contents() );
		}
		ob_end_clean();

	}
	
	function dbug_backtrace() {
		
		ob_start();
		if ( Kint::enabled() ) {
			call_user_func_array( array( 'Kint', 'trace' ), func_get_args() );
			dbug_add_note( ob_get_contents() );
		}
		ob_end_clean();
		
	}
	
	function dbug_add_note( $note ) {
		global $dbug_note;
		
		if (! isset($dbug_note) ) {
			// we don't register this action until someone tries to
			// make a debug report...
			$dbug_note = '';
			add_action('wp_footer', 'dbug_report_in_footer', 9999);
			add_action('admin_footer', 'dbug_report_in_footer', 9999);
		}
				
		$dbug_note .= "\n" . $note;
	}
	
	function dbug_report_in_footer() {
		global $dbug_note;
		global $DBug;
		global $wp_query;
		
		if (is_object($DBug)) {
			dbug_report( $wp_query );
			dbug_report( $_SERVER );
			$DBug->add('<h2>Debug Report</h2>');
			$DBug->add($dbug_note);
		}
	}
}
