# EFC Debug Plugin

- Contributors: efc
- Tags: debugging
- Requires at least: 3.5
- Tested up to: 4.4
- Stable tag: master
- License: The MIT License 
- License URI: http://opensource.org/licenses/MIT

This is a very simple plugin to facilitate the display of debugging messages. Nothing fancy here.

## Description

This plugin will place a small menu with an bolt (⏣) toward the right side of the admin bar. Clicking on this menu reveals a hidden div where debug messages are available.

### Features

- Recognizes a variable number of arguments.

- This plugin distinguishes between strings and other variables being reported. Any non-string will automatically be dumped with `print_r()` so that all elements can be seen.

- The bolt in the admin bar will be on a red background if `WP_DEBUG` is true. This is an extra reminder to turn off debugging before putting code into production.

- Always provides a copy of `$wp_query` and `$ENV`.

### Usage

Include `efc-debug-include.php` somewhere at the top of your functions.php (or other PHP file)...

```
include_once(ABSPATH . 'wp-content/plugins/efc-debug/efc-debug-include.php');
```

...note that you may have to edit that line to include the proper reference to the file.

Then anywhere else in your code use this to add items to the debug report...

```
if (function_exists('dbug_report')) dbug_report('message: ',$variable);
```

Include as many arguments as you need. Any non-string variable you include will be expanded to reveal its elements.

Don't forget you can use PHP magic constants too:

```
if (function_exists('dbug_report')) dbug_report(__CLASS__, __METHOD__);
```

You can add a backtrace with:

```
if (function_exists('dbug_backtrace')) dbug_backtrace();
```


When looking at your page, you can reveal the debug report by clicking on the bolt in the admin bar.

## Installation

The easiest way to install and maintain this plugin is with [GitHub Updater](https://github.com/afragen/github-updater). The great thing about using GitHub Updater is that the plugin will then be able to be updated through the regular WordPress update process.

Of course, if you wish to install and update the plugin manually, you are welcome to do so.

### Installing with GitHub Updater
1. First make sure you have [GitHub Updater](https://github.com/afragen/github-updater) itself installed.
2. In the WordPress Dashboard go to *Settings > GitHub Updater > Install Plugin*.
3. Enter `tenseg/efc-debug` as the Plugin URI and make sure the Remote Repository Host is `Bitbucket`. Then click the "Install Plugin".
4. Activate the EFC Debug plugin after it has installed.

When the plugin is updated, GitHub Updater will make sure that WordPress knows this and presents the usual offers to update the plugin for you.


### Installing manually
1. Download the [master.zip](https://bitbucket.org/tenseg/efc-debug/get/master.zip) file from the EFC Debug repository.
2. This file will have an odd name. Rename it to `efc-debug.zip`.
3. In the WordPress Dashboard go to *Plugins > Add New* and click on the small "this page" link offering to upload a plugin in .zip format.
4. Use the "Choose File" button and choose the "efc-debug.zip" file you just renamed.
5. Activate the EFC Debug plugin after it has installed.

When the plugin is updated, you will have to go through this same process to download and install the new version.

## Notes

This is really a plugin for developers. It will be of little use to regular WordPress users.

In fact, you should probably deactivate this plugin whenever you are not actively working on your site's code. Look for the bolt in the admin bar as a signal that the plugin is active. 

## Credits

This plugin includes a version of the [Kint](http://raveren.github.io/kint/) PHP debugger authored by Rokas Šleinius and originally designed by Mindaugas Stankaitis. 

## FAQ

### Aren't there better debugging plugins around?

Sure! Many people swear by [Debug Bar](https://wordpress.org/plugins/debug-bar/). Go check it out. For me, though, it was just too complex. I wanted something simple.

## Changelog

#### 1.5
* changed the umbrella to a bolt, since WordPress replaced the umbrella with emoji
* non-change (testing commits once again)

#### 1.4
* integrated Kint PHP debugger

#### 1.3
* added jquery enqueue just in case

#### 1.2
* include umbrella on admin pages

#### 1.1
* introduced the umbrella

#### 1.0
* first version