<?php
/*
Plugin Name:            Eric's Debug Menu
Description:            Puts a simple debug umbrella in the Admin bar of admin users.
Plugin URI:             https://bitbucket.org/tenseg/efc-debug/overview
Bitbucket Plugin URI:   https://bitbucket.org/tenseg/efc-debug
Bitbucket Branch:       master
Version:                1.4.5
Author:                 Eric Celeste
Author URI:             http://eric.clst.org/
License:                The MIT License 
License URI:            http://opensource.org/licenses/MIT
*/

/*
Copyright (c) 2015, Eric Celeste

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
	History:
	v.1.0    130922 (efc) beginning
	v.1.1    131113 (efc) use the umbrella
	v.1.2    131116 (efc) now included on admin pages as well
	v.1.2.1  150410 (efc) MIT License
	v.1.2.2               added CHANGES.md
	v.1.3    140526 (efc) added jquery enqueue just in case
	v.1.3.1  150410 (efc) demonstrate update for Alex
	v.1.3.2  150411 (efc) move to Tenseg's Bitbucket
	v.1.4    150425 (efc) integrated Kint (http://raveren.github.io/kint/)
	v.1.4.1  150426 (efc) cleaned up some of the instructions
	                      added import of efc-debug-include.php here
	v.1.4.2  150427 (efc) added Kint theme and config
	v.1.4.3  150427 (efc) removed debugging code (grrr)
	v.1.4.4  151227 (efc) make debugger only show for admin
						  change definition of admin to manage_options
	v.1.4.5  160121 (efc) trying a bolt symbol
						  since the umbrella was messed up by WordPress
*/


include_once('efc-debug-include.php');

if (! class_exists('EFC_Debug')) {
class EFC_Debug {

	function EFC_Debug() {
		$this->version = '1.4.2';
		$this->message = '';
		$this->symbol = json_decode('"\u23E3"'); // a bolt
		add_action( 'admin_bar_menu', array( $this, 'menu' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
		add_action( 'wp_footer', array( $this, 'render' ), 10999 );
		add_action( 'admin_enqueue_scripts', array( $this, 'scripts' ) );
		add_action( 'admin_footer', array( $this, 'render' ), 10999 );
		// wp_enqueue_script('jquery');
	}
	
	function add( $message ) {
		$this->message .= $message;
	}
	
	function menu() {
		global $wp_admin_bar;
		
		$menu_id = ( WP_DEBUG ? 'efc_debug_menu_wp_debug' : 'efc_debug_menu' );
		
		$wp_admin_bar->add_menu( array(
			'id'   => $menu_id,
			'parent' => 'top-secondary',
			'meta' => array( 'onclick' => 'jQuery("#efc-debug").toggle()' ),
			'title' => $this->symbol,
			'href' => '#' 
		) );
	}
	
	function scripts() {
		wp_enqueue_style( 
			'efc-debug', // handle
			plugins_url().'/efc-debug/efc-debug.css', // source in plugins
			false, // dependencies
			$this->version, // version
			'screen' // media
		);	
	}
	
	function render() {
		global $wp_query;
		
		if ( ! $this->message ) {
			$wpq = htmlspecialchars(print_r($wp_query,true));
			$env = htmlspecialchars(print_r($_ENV,true));
			$this->message = <<<MESSAGE
<p>Always check for the existance of the dbug_report function before using it:</p>

<pre>if (function_exists('dbug_report')) dbug_report('text',\$variable);</pre>

<pre>if (function_exists('dbug_backtrace')) dbug_backtrace();</pre>

<p>This way you won't choke your code when you disable the plugin.</p>

<p>If the Debug menu in the Admin Bar is red, that just means the WP_DEBUG is defined. It serves as a reminder in case you want to turn that off before going into production.</p>

<p>If you need to report to the plugin before the plugin is loaded, just this early in your code:</p>

<pre>include_once(dirname(__FILE__).'/../../plugins/efc-debug/efc-debug-include.php');</pre>

<p>But note that your particular file structure may require editing that line a bit.</p>

MESSAGE;
			ob_start();
			if ( Kint::enabled() ) {
				d( $wp_query );
				d( $_SERVER );
				$this->message .= ob_get_contents();
			}
			ob_end_clean();
		}
		$message = json_encode($this->message);
		echo <<<SCRIPT
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(document.body).append('<div id="efc-debug"><div id="efc-debug-inner">'+$message+'</div></div>');
});
</script>
SCRIPT;
	}

}
}

add_action( "init", "efc_debug_init" );
function efc_debug_init() {
	global $DBug;
	
	if ( !current_user_can('manage_options') )
		return; // don't show the debug menu to non-admin users
		
	if ( ! is_object($DBug) ) {
		$DBug = new EFC_Debug();
	}
}
