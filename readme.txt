=== EFC Debug Plugin ===
Contributors: efc
Tags: debugging
Requires at least: 3.5
Tested up to: 4.4.1
Stable tag: master
License: The MIT License 
License URI: http://opensource.org/licenses/MIT
Donate link: https://supporters.eff.org/donate
Bitbucket Plugin URI: tenseg/efc-debug

This is a very simple plugin to facilitate the display of debugging messages. Nothing fancy here.

== Description ==

This plugin will place a small menu with an umbrella toward the right side of the admin bar. Clicking on this menu reveals a hidden div where debug messages are available.

= Features =

Recognizes a variable number of arguments.

This plugin distinguishes between strings and other variables being reported. Any non-string will automatically be dumped with Kint so that all elements can be seen.

The umbrella in the admin bar will be on a red background if WP_DEBUG is true. This is an extra reminder to turn off debugging before putting code into production.

Always provides a copy of $wp_query and $ENV.

= Usage =

Include 'efc-debug-include.php' somewhere at the top of your functions.php (or other PHP file)...

`include_once(dirname(__FILE__).'/../../plugins/efc-debug/efc-debug-include.php');`

...note that you may well have to edit that line to include the proper reference to the file.

Then anywhere else in your code use this to add items to the debug report...

`if (function_exists('dbug_report')) dbug_report('message: ',$variable);`

You can also request a stack trace by using...

`if (function_exists('dbug_report')) dbug_backtrace();`

When looking at your page, you can reveal the debug report by clicking on the umbrella in the admin bar.

= Example =

Put this (edited for your directory structure) near the top of your code:
`include_once(dirname(__FILE__).'/../../plugins/efc-debug/efc-debug-include.php');`

Include as many arguments as you need:
`if (function_exists('dbug_report')) dbug_report('message: ',$variable);`

Don't forget you can use PHP magic constants too:
`if (function_exists('dbug_report')) dbug_report(__CLASS__, __METHOD__);`

= Notes =

This is really a plugin for developers. It will be of little use to regular WordPress users.

In fact, you should probably deactivate this plugin whenever you are not actively working on your site.

= Credits =

This plugin includes a version of the [Kint](http://raveren.github.io/kint/) PHP debugger authored by Rokas Šleinius and originally designed by Mindaugas Stankaitis. 

== Installation ==

To install the plugin follow these steps :

1. Download the efc-debug.zip file to your local machine.
1. Unzip the file.
1. Upload the "efc-debug" folder to your "/wp-content/plugins/" directory.
1. Activate the plugin through the 'Plugins' menu in WordPress (but only when you really need to be debugging).

== Frequently Asked Questions ==

= Aren't there better debugging plugins around? =

Sure! Many people swear by [Debug Bar](https://wordpress.org/plugins/debug-bar/). Go check it out. For me, though, it was just too complex. I wanted something simple.

== Upgrade Notice ==

= 1.4 =
Integrates Kint for better navigation of complex variable dumps and adds the ability to request stack traces.

== Changelog ==

= 1.4 =
* integrated Kint PHP debugger
* added dbug_backtrace() to capture stack traces
* added import of include file to simplify use (1.4.1)
* added special theme and config for Kint (1.4.2)
* fixed a stupid mistake (1.4.3)
* changed definition of admin to manage_options (1.4.4)
* avoided printing debug info (1.4.4)
* replace umbrella with bolt symbol (1.4.5)

= 1.3 =
* added jquery enqueue just in case
* demo update (1.3.1)
* move to Bitbucket (1.3.2)
* new readme (1.3.2)
* readme for new GitHub Uploader (1.3.3)

= 1.2 =
* include umbrella on admin pages
* moved to MIT Licence (1.2.1)
* added CHANGES.md (1.2.2)

= 1.1 =
* introduced the umbrella

= 1.0 =
* first version

