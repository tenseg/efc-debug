<?php
isset( $GLOBALS['_kint_settings'] ) or $GLOBALS['_kint_settings'] = array();
$_kintSettings = &$GLOBALS['_kint_settings'];


/** @var string name of theme for rich view */
$_kintSettings['theme'] = 'efc-debug';

/**
 * @var callable|null
 *
 * @param array $step each step of the backtrace is passed to this callback to clean it up or skip it entirely
 *
 * @return array|null you can return null if you want to bypass outputting this step
 *
 * [!] EXAMPLE:
 *
 * $_kintSettings['traceCleanupCallback'] = function( $traceStep ) {
 *      if ( isset( $traceStep['class'] ) && strtolower( $traceStep['class'] ) === 'errorHandler' ) {
 *           return null;
 *      }
 *
 *      if ( isset( $traceStep['function'] ) && strtolower( $traceStep['function'] ) === '__tostring' ) {
 *          $traceStep['function'] = "[object converted to string]";
 *      }
 *
 *       return $traceStep;
 * };
 */
 

$_kintSettings['traceCleanupCallback'] = function( $traceStep ) {
	// exclude anything from our efc-debug functions ...
    if ( isset( $traceStep['file'] ) ) {
		$test = 'efc-debug-include.php';
		$testlen = strlen($test);
		$filelen = strlen($traceStep['file']);
		if ( $filelen > $testlen ) {
			if ( substr_compare($traceStep['file'], $test, $filelen - $testlen, $testlen) === 0 ) {
				return null;
			}
		}
    }
    // exclude anything from Kint itself
    if ( isset( $traceStep['class'] ) && strtolower( $traceStep['class'] ) === 'kint' ) {
        return null;
    }
    // otherwise, continue
    return $traceStep;
};



unset( $_kintSettings );